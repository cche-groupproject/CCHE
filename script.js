//FOOTER SWITCH
{
var fcounter = 0; //variable outside of function so it doesn't get reset on each onclick event
function switchfooter(){

var	main = document.getElementById("main");
var	footer = document.getElementById("footer");

	if (fcounter === 0){//if counter is 0 (default) then expand footer
		main.style.marginBottom = "100px";
		footer.style.height = "100px";
		document.getElementById("footerImage").src = "images/siteImages/down.png";
		document.getElementById("footerImage").title = "Press the arrow to hide the footer.";
		fcounter++;
	} else if (fcounter === 1) {//if counter is 1 hide footer
		main.style.marginBottom = "20px";
		footer.style.height = "20px";
		document.getElementById("footerImage").src = "images/siteImages/up.png";
		document.getElementById("footerImage").title = "Press the arrow to extend the footer.";
		fcounter--;
	} 
}
}
//END OF FOOTER SWITCH


//START OF SIDE BAR MENU
{
var sbtimeout = 2500;
var sbclosetimer = 0;
var sbitem = 0;  

function sidebaropen(id) {
	if (sbitem) sbitem.style.left = '-170px';
	if (sbitem) sbitem.style.zIndex = '4';
	sbitem = document.getElementById(id);
	sbitem.style.left = "-5px";
	sbitem.style.zIndex = '5';
}

function sidebarclose(){
	if (sbitem) sbitem.style.left = '-170px';
	if (sbitem) sbitem.style.zIndex = '4';
}

function sbclosetime() {
	sbclosetimer = setTimeout(sidebarclose, sbtimeout);
}

function sbcancelclosetime(){
	if (sbclosetimer) {
		sbclosetimer = clearTimeout(sbclosetimer);
		};
}
}
//END OF SIDEBAR MENU


//START OF SLIDESHOW
{
// generic slideshow arrays
var slideIndex = 0; // current slideshow object
var playTimer = 0; // timer for sldeshow
var timerCounter = 0; // on/off switch to toggle slideshow
var x = document.getElementsByClassName("slideshow"); // array containing all slideshow objects


// video related arrays
var videoPlayTimer = 0;
var pauseTimer = 0; // stores current time of video when paused to be deducted from total time when played again
var videoTime = 0; // holds overall duration of video and swaps to next slideshow objext when complete
var videoVolume = 0; // // holds video volume status (0 for mute, 0.75 is on but not full volume)
var videoTimer = 0; // timer for showing current video duration and time passed

function playSShow() { // used to auto play slideshow
	control(+1);
};

function control(n){ // used as next and previous slide controls
	if (timerCounter === 0) { // reset slide timer
		{playTimer = clearTimeout(playTimer)};
		{playTimer = setTimeout(playSShow, 10000)};
		showImg(slideIndex += n);
	} else if (timerCounter === 1) {
		showImg(slideIndex += n);
	};
};

function toggleSShow() {
	if (timerCounter === 0) {
		$('.toggleSS').prop('title','Start slideshow' );
		playTimer = clearTimeout(playTimer);
		timerCounter++;
	} else {
		$('.toggleSS').prop('title','Stop slideshow');
		playTimer = setTimeout(playSShow, 10000);
		timerCounter--;
	};
};

function showImg(n) {
	var i;
	if (x.length !== 0) {
		if (n > x.length) {slideIndex = 1};
		if (n < 1) {slideIndex = x.length};
		for (i = 0; i < x.length; i++) {
			x[i].style.display = "none";
			x[i].volume = 0;
		};
		x[slideIndex-1].style.display = "block";
	};	
	videoTimeStart(); // starts displaying video time stats
	if (x.length >= 1) {
		if (x[slideIndex-1].nodeName === "VIDEO") { // tests if object is video
			x[slideIndex-1].volume = videoVolume;
			if (videoVolume !== 0) {
				$('#ssUnmute').removeClass('hidden');
				$('#ssMute').addClass('hidden');
			} else {
				$('#ssUnmute').addClass('hidden');
				$('#ssMute').removeClass('hidden');
			};
			$('.ssSound').fadeIn(500); // show sound option
			playVideo();
			$('#ssPause').fadeIn(500);
			$('#ssPlay').fadeOut(500);
			$('.ssPause').fadeIn(500);
			
		} else {
			$('.ssSound').fadeOut(500); // hide sound option if not a video
			$('.ssPause').fadeOut(500);
		};
	};
};

function playVideo () {
	if (x[slideIndex-1].readyState > 3) {
		videoPlayTimer = clearTimeout(videoPlayTimer);
		x[slideIndex-1].currentTime = 0; // goes to start of the video
		if (timerCounter == 0) { // if timerCounter switch is off don't go to next slideshow object
			playTimer = clearTimeout(playTimer); // clears current timeout, replaces with video duration
			pauseTimer = x[slideIndex-1].currentTime * 1000;
			videoTime = x[slideIndex-1].duration * 1000; // fill variable with video length so next slideshow comes on automatically, times seconds from video by millisecond javascript standard
			playTimer = setTimeout(playSShow, videoTime - pauseTimer ); // set new timer for next slideshow object
		};
		x[slideIndex-1].play(); // play video
	} else {
		videoPlayTimer = setTimeout(playVideo, 500);
	};
};

function videoTimeStart() { // display video time stats
	if (x.length >= 1) {
		if (x[slideIndex-1].nodeName === "VIDEO") {
			var ctMinutes = Math.floor(x[slideIndex-1].currentTime/60); // set variable of current time to pass to maths function setTime//
			var ctSeconds = Math.round(x[slideIndex-1].currentTime%60); // set variable of current time to pass to maths function setTime//
			var vdMinutes = Math.floor(x[slideIndex-1].duration/60); // set variable of current time to pass to maths function setTime
			var vdSeconds = Math.round(x[slideIndex-1].duration%60); // set variable of current time to pass to maths function setTime
			// below makes outputted number 2 digits
			if (ctMinutes <= 9) ctMinutes = '0' + ctMinutes;
			if (ctSeconds <= 9) ctSeconds = '0' + ctSeconds;
			if (vdMinutes <= 9) vdMinutes = '0' + vdMinutes;
			if (vdSeconds <= 9) vdSeconds = '0' + vdSeconds;
			$('#ssTime').text(ctMinutes + ':' + ctSeconds + ' - ' + vdMinutes + ':' + vdSeconds); // set the current time and video duration to 2 decimal places
			$('#ssTime').fadeIn(500); // show duration of video and current time of video
			videoTimer = setTimeout(videoTimeStart, 1000); // repeat countdown every second
			if (x[slideIndex-1].currentTime == x[slideIndex-1].duration) {
				if (timerCounter == 1) { // show video repeat button 
					$('.ssRepeat').show();
				};
			} else {
				$('.ssRepeat').hide();
			};
		} else {
			$('#ssTime').fadeOut(500); // hide time
		};
	};
};

function toggleVideo () {
	if (x[slideIndex-1].paused) {
		if (timerCounter == 0) { // if timerCounter switch is off don't go to next slideshow object
			pauseTimer = x[slideIndex-1].currentTime * 1000;
			videoTime = x[slideIndex-1].duration * 1000; // fill variable with video length so next slideshow comes on automatically, times seconds from video by millisecond javascript standard
			playTimer = setTimeout(playSShow, videoTime - pauseTimer ); // set new timer for next slideshow object
		};
		x[slideIndex-1].play();
		$('#ssPlay').hide(500);
		$('#ssPause').show(500);
	} else {
		playTimer = clearTimeout(playTimer);
		x[slideIndex-1].pause();
		$('#ssPlay').show(500);
		$('#ssPause').hide(500);
	};
};

function repeatVideo () {
	$('.ssRepeat').hide();
	playVideo();
};

function toggleSound() {
	if (videoVolume == 0) { // determines whether to mute volume or not
		videoVolume = 0.75;
		$('#ssUnmute').toggleClass('hidden');
		$('#ssMute').toggleClass('hidden');
	} else {
		videoVolume = 0;
		$('#ssUnmute').toggleClass('hidden');
		$('#ssMute').toggleClass('hidden');
	};
	x[slideIndex-1].volume = videoVolume;
};
}
//END OF SLIDESHOW


//START OF ACCESSIBILITIES
{
function swapStyleSheet(sheet){
	document.getElementById('pagestyle').setAttribute('href', sheet);
}
}
//END OF ACCESSILIBITIES


//START OF SEARCH BAR
{
$(document).on("keyup","#mainSearch",function () {
	var searchQuery = $(this).val();
	if (searchQuery == "") {
		$("#searchResults").hide();
	} else {
		$("#searchResults").show();
	}
});
$(document).on("mouseout","#mainSearch",function () {
	$("#searchResults").hide();
});
$(document).on('click', function () {
	$("#searchResults").hide();
});
}
//END OF SEARCH BAR


//START OF NEWS CONTENT
{
function showContent(id) {
	var article = document.getElementById(id);
	var Button = document.getElementById(id + "Button");
	
	article.style.height = "600px";
	$(article).children(".newsContent").css("height","500px")
	$(Button).css("opacity",0);
};
}
//END OF NEWS CONTENT


//START OF ADVERTS
{
	var advertTimer = 0;
	var advertHIndex = 0;
	var advertWIndex = 0;
	
function advertSS(){
	{advertTimer = clearTimeout(advertTimer)};
	showAdvertH(advertHIndex += 1);
	showAdvertW(advertWIndex += 1);
	{advertTimer = setTimeout(advertSS, 10000)};
}

function showAdvertH(n) {
	var c;
	var z = document.getElementsByClassName("advertH");
	if (z.length !== 0) {
		if (n > z.length) {advertHIndex = 1};
		if (n < 1) {advertHIndex = z.length};
		for (c = 0;c < z.length; c++) {
			z[c].style.display = "none";
		};
		z[advertHIndex-1].style.display = "block";
	};
}

function showAdvertW(n) {
	var d;
	var y = document.getElementsByClassName("advertW");
	if (y.length !== 0) {
		if (n > y.length) {advertWIndex = 1};
		if (n < 1) {advertWIndex = y.length};
		for (d = 0; d < y.length; d++) {
			y[d].style.display = "none";
		};
		y[advertWIndex-1].style.display = "block";
	}
}
}
//END OF ADVERTS


//START OF NAVIGATION DROPDOWN MENU
{
var currentMenu = "";
var menuTimer = "";
function showNavMenu(menu){
	if (currentMenu == "#" + menu) 
	{
		$(currentMenu).toggle();
		clearTimeout(menuTimer);
		menuTimer = setTimeout(function(){$(currentMenu).hide();}, 5000);
	} else {
		$(currentMenu).hide();
		currentMenu = "#" + menu;
		$(currentMenu).toggle();
		clearTimeout(menuTimer);
		menuTimer = setTimeout(function(){$(currentMenu).hide();}, 5000);
	};
};

$(document).on("mouseover",".navDD", function() {
	clearTimeout(menuTimer);
	menuTimer = setTimeout(function(){$(currentMenu).hide();}, 5000);
});
}
//END OF NAVIGATION DROPDOWN MENU


// START OF DASHBOARD
{
function showDashHowTo () { // shows dahsboard how to
	$('#dashHowToBK').fadeIn(1000);
	$('#dashHowTo').fadeIn(1000);
};

$(document).on('click','#dashHowTo', function () { // shows dashboard again
	$('#dashHowToBK').fadeOut(1000);
	$('#dashHowTo').fadeOut(1000);
});

$(document).on('click','#dashHowToBK', function () { // shows dashboard again
	$('#dashHowToBK').fadeOut(1000);
	$('#dashHowTo').fadeOut(1000);
});
	
var currentDash = ""; //keeps track of current dash item on show
function showDashContent(id) {  //swaps between the different dash options (projects/deadlines etc.)
	if (currentDash == "#" + id) //if current dash is same as dash item give through function:
	{
		$(currentDash).toggle(); //hide or show dash list and content
		$("#dashContent").toggle(); 
	} else { 
		$(currentDash).hide(); //hide old dash list and content
		currentDash = "#" + id; //loads new dash item into variable
		$(currentDash).show(); //show new dash content item
	};
};

$(document).on("click",".dashButton",function(){ // highlights current dash item selected
	$('#dashMenu').children(".dashButton").removeClass("dashMenuSelected");
	$(this).addClass("dashMenuSelected");
});

var currentDashList = ""; //keeps track of current dash list on show
function showDashList(id) { // shows dash list selected
	if (currentDashList == "#" + id) { // evualates if item was last selected
		$(currentDashList).toggle(); // shows/hides dash list
		$('#dashContentContainer').toggle(); // shows/hides dash content
	} else {
		$(currentDashList).hide(); //hides last dash list
		$(currentDash).hide(); // hides last dash content
		currentDash = null;
		currentDashList = "#" + id; // fills vairable with new dash list
		$(currentDashList).show(); // shows current dash list
		$('#dashContentContainer').show(); // shows dash content container
	};
};

$(document).on('dblclick','.dashButton', function() { // when dash item is double clicked send user to that page
	if ($(this).text().toLowerCase() == "deadlines") { // if dash item was deadlines forward them to courses page
		window.location("courses.html");
	} else {
		window.location($(this).text().toLowerCase() + ".html");
	}
});

// START OF DASH DEADLINES
{
// All functions to show selected options and hide other options from it's opposite selection
$(document).on("click", ".dashServerFile", function () {
	$(this).next(".dashLocalFile").css("background","#286090");
	$(this).css("background","#888888");
	$(".dashDeadlineExistingFile").show();
	$(".dashDeadlineNewFile").hide();
});

$(document).on("click", ".dashLocalFile", function () {
	$(this).prev(".dashServerFile").css("background","#286090");
	$(this).css("background","#888888");
	$(".dashDeadlineExistingFile").hide();
	$(".dashDeadlineNewFile").show();
});

$(document).on("click", ".dashDeadlineUploadSaveFile", function () {
	$(this).next("button").css("background","#286090");
	$(this).css("background","#888888");
	$(".dashDeadlineUploadNewFile").hide();
	$(".dashDeadlineUploadSelectFile").show();
});

$(document).on("click", ".dashDeadlineUploadFile", function () {
	$(this).prev("button").css("background","#286090");
	$(this).css("background","#888888");
	$(".dashDeadlineUploadSelectFile").hide();
	$(".dashDeadlineUploadNewFile").show();
});

$(document).on("click", ".dashDeadlineUploadSaveNewFileVersion", function () {
	$(this).next("button").css("background","#286090");
	$(this).css("background","#888888");
	$(".dashDeadlineUploadSaveNewFileSubversionDiv").hide();
	$(".dashDeadlineUploadSaveNewFileVersionDiv").show();
});

$(document).on("click", ".dashDeadlineUploadSaveNewFileSubversion", function () {
	$(this).prev("button").css("background","#286090");
	$(this).css("background","#888888");
	$(".dashDeadlineUploadSaveNewFileVersionDiv").hide();
	$(".dashDeadlineUploadSaveNewFileSubversionDiv").show();
});
}
// END OF DASH DEADLINES

$(document).on("change", ".dashFileVersion", function () {
	$(".dashFileVersion").show();
});

$(document).on("change", ".dashFileSubversion", function () {
	$(".dashFileComment").show();
});

$(document).on("click","input[name='searchPublic']", function () { // disables/enables checkbox for if friends can search file
	searchFriend = $(this).closest("tr").next("tr").find("input[name='searchFriend']:first");
	if ($(this).prop('checked') == false) {
		searchFriend.prop("disabled", false);
	} else {
		searchFriend.prop("disabled", true);
		searchFriend.prop("checked", true);
	};
})


}
//END OF DASHBOARD 

// START OF PROJECTS -- APPLIES TO DASH BOARD AS WELL
{
var currentProjectList = 0 ;
$(document).on('click','.projectMenuButtons button', function() {
	$(currentProjectList).hide();
	$('.projectMenuButtons button').removeClass('projectMenuSelected');
	currentProjectList = $(this).text().replace("Project ",".p");
	$(currentProjectList).show();
	$(this).addClass('projectMenuSelected');
	
});

var currentAdmin = 0;
var adminStatus = 0;
$(document).on('click','a.projectMakeAdmin', function() {
	currentAdmin = $(this);
	$('#projectConfirmAdminBK').show();
	$('#projectConfirmAdmin').show();
	var adminName = $(this).prev('span').text();
	if ($(this).text() == "✓") {
		adminStatus = true;
	} else {
		adminStatus = false;
	};
	$('#adminName').text(adminName);
	if (adminStatus == true) {
		$('#adminDecision').html('NOT BE ');
	} else {
		$('#adminDecision').html('BE ');
	}
});

$(document).on('click','#projectAdminConfirmButtons button', function () {
	var response = '';
	if ($(this).text() == 'Yes') {
		if (adminStatus == false) {
			response = '✓';
		} else {
			response = '';
		};
		currentAdmin.text(response);
	} else {
		if (adminStatus == false) {
			response = '';
		} else {
			response = '✓';
		};
		currentAdmin.text(response);
	};
	$('#projectConfirmAdminBK').hide();
	$('#projectConfirmAdmin').hide();
});

var currentSetting = 0;
var settingStatus = 0;
$(document).on('click','a.projectSettingControl', function() {
	currentSetting = $(this);
	$('#projectConfirmSettingsBK').show();
	$('#projectConfirmSettings').show();
	var settingName = $(this).prev('span').text();
	if ($(this).text() == "✓") {
		settingStatus = true;
	} else {
		settingStatus = false;
	};
	$('#settingName').text(settingName);
	if (settingStatus == true) {
		$('#settingDecision').html('NOT BE ');
	} else {
		$('#settingDecision').html('BE ');
	}
});

$(document).on('click','#projectSettingConfirmButtons button', function () {
	var response = '';
	if ($(this).text() == 'Yes') {
		if (settingStatus == false) {
			response = '✓';
		} else {
			response = '';
		};
		currentSetting.text(response);
	} else {
		if (adminStatus == false) {
			response = '';
		} else {
			response = '✓';
		};
		currentSetting.text(response);
	};
	$('#projectConfirmSettingsBK').hide();
	$('#projectConfirmSettings').hide();
});
}
//END OF PROJECTS

//START OF COURSES
{
$(document).ready(function() {
	$('.course').hide();
	$('.course-title').click(function() {
		$(this).next().slideToggle('slow');
		$(this).toggleClass("module-title-toggle");
	});
});
}
//END OF COURSES

//thankyou fuction 
function randomNumber(min, max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
    }
function generateCaptcha() {
        $('#captcha').html([randomNumber(1, 20), '+', randomNumber(1, 20), '='].join(' '));
    }
$(document).ready(function() {
    // Generate a simple captcha
    generateCaptcha();
	});

var messageSent=false;
	
$(document).on('submit','#contactForm', function () {
	if(messageSent){
		$('#sentWarning').show();
		$('#thankYouMessage').hide();
	}else{
		var items = $('#captcha').html().split(' ');
		sum = parseInt(items[0]) + parseInt(items[2]);
		var answer = $('input[name="captchaAnswer"]').val();
		if(sum== answer){
			$('#thankYouMessage').show();
			$('#captchaWrongMessage').hide();
			messageSent=true;
		}else{
			$('#captchaWrongMessage').show();
			generateCaptcha();
			$('#thankYouMessage').hide();
		};
	};
	return false;
});












