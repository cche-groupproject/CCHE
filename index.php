<?php
require_once ('config.php');
?>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta name="Author" content="CCHE" />
	<meta name="Description" content="Higher Education of Chesterfield College" />
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	
	<title>Chesterfield College Higher Education</title>

	<link id="pagestyle" rel="stylesheet" type="text/css" href="css.css" />
	<script type="text/javascript" src="jqscript.js"></script>
	<script type="text/javascript" src="script.js"></script>
</head>



<body onload="playSShow(); advertSS()">
	<div class="header"> <!-- Start of header -->
		<div class="navBar"> <!-- Start of navigational bar -->
			<ul id="mainMenu"> <!-- List to contain dropdown menu -->
				<li id="sMainMenu"> 
					<form class="fsearch" action="search.php">
						<input id="mainSearch" type="search" placeholder="Search!" name="mainSearch"/>
						<input type="submit" id="msearch" value="Search!">
					</form>
					<div id="searchResults">
					</div>
				</li>
				<li id="lMainMenu" onclick="showNavMenu('mainMenuDD')"> 
					<a href="./index.html"> 
						<img src="images/siteLogo.png" alt="logo" class="logo" title="Chesterfield College Higher Education Logo" />
					</a>
						<img class="menuArrow" src="images/siteImages/down.png" />
					<span id="navLinkID">
						Menu
					</span>
						<img class="menuArrow" src="images/siteImages/down.png" />
					<div id="mainMenuDD" class="navDD hidden">
						<a href="index.html">Homepage</a>
						<a href="abouthe.html">About HE</a>
						<a href="courses.html">Courses</a>
						<a href="news.html">News</a>
						<a href="recources.html">Resources</a>
						<a href="contactus.html">Contact us</a>
					</div>
				</li>
				<li id="rMainMenu" onclick="showNavMenu('accountMenuDD')">
						<img class="menuArrow" src="images/siteImages/down.png" />
					<span id="navLinkID">
						My Hub
					</span>
						<img class="menuArrow" src="images/siteImages/down.png" />
					<a href="./dashboard.html">
						<img class="profileImage" src="images/user/userIcon.png" />
					</a>
					<div  id="accountMenuDD" class="navDD hidden">
						<a href="dashboard.html">Sign in</a>
						<a href="">Files</a>
						<a href="">Messages</a>
						<a href="news.html">Forum Center</a>
						<a href="projects.html">Projects</a>
						<a href="">Notifications</a>
					</div>
				</li>
			</ul>
		</div> <!-- End of navigational bar items -->
	</div> <!-- End of header -->
	<div id="main">
		<div id="sidebar"> <!-- Start of sidebar menu -->
			<div class="sidebarContainer" id="sb1" 
			onclick="sidebaropen('sb1')"
			onmouseover="sbcancelclosetime()"
			onmouseout="sbclosetime()">
				<p class="sidebartext">Accessibility</p>
				<div class="sidebar">
					<button class="icon" id="accBtn2" onclick="swapStyleSheet('css.css')" title="Default">A</button>
					<button class="icon" id="accBtn1" onclick="swapStyleSheet('hcss.css')" title="High Contrast">H</button>
					<button class="icon" id="accBtn3">A</button>
					<button class="icon" id="accBtn3">A</button>
				</div>
			</div>
			<div class="sidebarContainer"  id="sb2" 
			onclick="sidebaropen('sb2')"
			onmouseover="sbcancelclosetime()"
			onmouseout="sbclosetime()">
				<p class="sidebartext">My Courses</p>
				<div class="sidebar">
					<button class="icon" id="accBtn2" onclick="swapStyleSheet('css.css')" title="Default">A</button>
					<button class="icon" id="accBtn1" onclick="swapStyleSheet('hcss.css')" title="High Contrast">H</button>
					<button class="icon" id="accBtn3">A</button>
					<button class="icon" id="accBtn3">A</button>
				</div>
			</div>
			<div  class="sidebarContainer" id="sb3" 
			onclick="sidebaropen('sb3')"
			onmouseover="sbcancelclosetime()"
			onmouseout="sbclosetime()">
				<p class="sidebartext">My Favourites</p>
				<div class="sidebar">
					<button class="icon" id="accBtn2" onclick="swapStyleSheet('css.css')" title="Default">A</button>
					<button class="icon" id="accBtn1" onclick="swapStyleSheet('hcss.css')" title="High Contrast">H</button>
					<button class="icon" id="accBtn3">A</button>
					<button class="icon" id="accBtn3">A</button>
				</div>
			</div>
		</div> <!-- End of sidebar menu -->
		<div id="twitterWidget">
			<a class="twitter-timeline" data-height="400" href="https://twitter.com/ChesterfieldAC">Tweets by ChesterfieldAC</a> <script async src="http://platform.twitter.com/widgets.js" charset="utf-8"></script>
		</div>
		<div id="slideshow">
			<video class="slideshow">
				<source src="videos/video1.mp4" type="video/mp4">
				<p>not recognised</p>
			</video>
			<video class="slideshow">
				<source src="videos/video2.mp4" type="video/mp4">
				<p>not recognised</p>
			</video>
			<img class="slideshow" src="images/ss/image1.png">
			<img class="slideshow" src="images/ss/image2.png">
			<img class="slideshow" src="images/ss/image3.png">
			<img class="slideshow" src="images/ss/image4.png">
			<img class="slideshow" src="images/ss/image5.png">
			<div class="slideshowButtons">
				<div class="ssButton" title="Previous slide" onclick="control(-1)">&#10094;</div>
				<div class="ssButton ssRight" title="Next slide" onclick="control(+1)">&#10095;</div>
				<div class="toggleSS" title="Stop slideshow" onclick="toggleSShow()"><img src='images/ss/controls/ssToggle.png' alt='toggle slideshow button' /></div>
				<div class="ssSound hidden" title="Unmute" onclick="toggleSound()"><img src='images/ss/controls/ssUnmuted.png' id="ssUnmute" class="hidden" title="Unmute" /><img src='images/ss/controls/ssMuted.png'id="ssMute" title="Mute"></div>
				<div class="ssPause"  onclick="toggleVideo()"><span id="ssPause" title="Pause video"><img src='images/ss/controls/ssPause.png' alt='Pause video button' /></span><span id="ssPlay" class="hidden" title="Play video"><img src='images/ss/controls/ssPlay.png' alt='Pause video button' /></span></div>
				<div class="ssRepeat" title="Repeat video" onclick="repeatVideo()"><img src='images/ss/controls/ssRepeat.png' alt='Replay video button' /></div>
				<span id="ssTime"></span>
			</div>
		</div>
		<div class="content shadow">
		<div class="advertHeight">
				<img class="advert advertH" src="images/adverts/advertH1.png" />
				<img class="advert advertH" src="images/adverts/advertH2.png" />
				<img class="advert advertH" src="images/adverts/advertH3.png" />
		</div>	
			<h1>This is header 1 - Welcome to Chesterfield College's Higher Education Portal</h1>
			<p><a href="">This is a visited hyperlink</a></p>
			<p><a href="./george.html">This is a hyperlink</a></p>

			<div id="news1" class="newsContainer shadow">
				<h2>This is a News Title</h2>
				<div class="newsControls">
					<img src="images/news.jpg" alt="News Picture" title="News Picture" />
					<a href="news.html"><button class="newsControl">Go to news story</button></a>
					<a href="news.html"><button class="newsControl">More of this category</button></a>
					<a href="news.html"><button class="newsControl">More from this author</button></a>
					<a href="news.html"><button class="newsControl">Add to favourites</button></a>
				</div>
				<div class="newsContent">
					<table>
						<tr class="newsInfo">
							<th>Author:</th><td>John Doe</td>
							<th>Category:</th><td>Test</td>
							<th>Date:</th><td>12/12/2012</td>
						</tr>
						<tr>
							<td colspan="6">
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
							</td>
						</tr>
					</table>
					<button id="news1Button" class="newsRM" onclick="showContent('news1')">Read More</button>
				</div>
			</div>
			<div id="news2" class="newsContainer shadow">
				<h2>This is a News Title</h2>
				<div class="newsControls">
					<img src="images/news.jpg" alt="News Picture" title="News Picture" />
					<a href="news.html"><button class="newsControl">Go to news story</button></a>
					<a href="news.html"><button class="newsControl">More of this category</button></a>
					<a href="news.html"><button class="newsControl">More from this author</button></a>
					<a href="news.html"><button class="newsControl">Add to favourites</button></a>
				</div>
				<div class="newsContent">
					<table>
						<tr class="newsInfo">
							<th>Author:</th><td>John Doe</td>
							<th>Category:</th><td>Test</td>
							<th>Date:</th><td>12/12/2012</td>
						</tr>
						<tr>
							<td colspan="6">
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
							</td>
						</tr>
					</table>
					<button id="news2Button" class="newsRM" onclick="showContent('news2')">Read More</button>
				</div>
			</div>	
			<div id="news3" class="newsContainer shadow">
				<h2>This is a News Title</h2>
				<div class="newsControls">
					<img src="images/news.jpg" alt="News Picture" title="News Picture" />
					<a href="news.html"><button class="newsControl">Go to news story</button></a>
					<a href="news.html"><button class="newsControl">More of this category</button></a>
					<a href="news.html"><button class="newsControl">More from this author</button></a>
					<a href="news.html"><button class="newsControl">Add to favourites</button></a>
				</div>
				<div class="newsContent">
					<table>
						<tr class="newsInfo">
							<th>Author:</th><td>John Doe</td>
							<th>Category:</th><td>Test</td>
							<th>Date:</th><td>12/12/2012</td>
						</tr>
						<tr>
							<td colspan="6">
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
							</td>
						</tr>
					</table>
					<button id="news3Button" class="newsRM" onclick="showContent('news3')">Read More</button>
				</div>
			</div>	
			<div id="news4" class="newsContainer shadow">
				<h2>This is a News Title</h2>
				<div class="newsControls">
					<img src="images/news.jpg" alt="News Picture" title="News Picture" />
					<a href="news.html"><button class="newsControl">Go to news story</button></a>
					<a href="news.html"><button class="newsControl">More of this category</button></a>
					<a href="news.html"><button class="newsControl">More from this author</button></a>
					<a href="news.html"><button class="newsControl">Add to favourites</button></a>
				</div>
				<div class="newsContent">
					<table>
						<tr class="newsInfo">
							<th>Author:</th><td>John Doe</td>
							<th>Category:</th><td>Test</td>
							<th>Date:</th><td>12/12/2012</td>
						</tr>
						<tr>
							<td colspan="6">
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
							</td>
						</tr>
					</table>
					<button id="news4Button" class="newsRM" onclick="showContent('news4')">Read More</button>
				</div>
			</div>
			<div id="news5" class="newsContainer shadow">
			<h2>This is a News Title</h2>
				<div class="newsControls">
					<img src="images/news.jpg" alt="News Picture" title="News Picture" />
					<a href="news.html"><button class="newsControl">Go to news story</button></a>
					<a href="news.html"><button class="newsControl">More of this category</button></a>
					<a href="news.html"><button class="newsControl">More from this author</button></a>
					<a href="news.html"><button class="newsControl">Add to favourites</button></a>
				</div>
				<div class="newsContent">
					<table>
						<tr class="newsInfo">
							<th>Author:</th><td>John Doe</td>
							<th>Category:</th><td>Test</td>
							<th>Date:</th><td>12/12/2012</td>
						</tr>
						<tr>
							<td colspan="6">
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
								<p>This is a paragraph</p>
							</td>
						</tr>
					</table>
					<button id="news5Button" class="newsRM" onclick="showContent('news5')">Read More</button>
				</div>
			</div>
		</div>
		
		<div class="advertWidth">
				<img class="advert advertW" src="images/adverts/advertW1.png" />
				<img class="advert advertW" src="images/adverts/advertW2.png" />
				<img class="advert advertW" src="images/adverts/advertW3.png" />
		</div>
		
		<div id="footer">
			<img id="footerImage" src="images/siteImages/up.png" alt="Control if you can see the footer." title="Press the arrow to extend the footer." onclick="switchfooter()">
			<div id="footerContent">
				<img class="smIcon" src="images/sm/fbicon.png" />
				<img class="smIcon" src="images/sm/ticon.png" />
				<img class="smIcon" src="images/sm/yticon.png" />
				<ul>
				<li>Sitemap</li>
				<li>Home</li>
				<li>Accessibility</li>
				<li>Contact us</li>
				</ul>
			</div>		
		</div>
	</div>
</body>
</html>